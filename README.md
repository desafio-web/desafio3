# Proposta Histórico Fatura

 

Primeiramente obrigado por estar aqui! E querer trabalhar no Itaú Unibanco!.

 

Esse é nosso desafio 3 de 3, nosso desafio final, onde iremos avaliar sua habilidade com html, css e javascript.

Para isso você irá consumir a API Java, realizada no desafio 2, onde estão os lançamentos de um cartão de credito dentro de um mesmo ano.

(exemplo conceitual https://dribbble.com/shots/5201742-Intermediary-Tool-Quotes-Page)

 

O método de avaliação é simples, tendo seu código em mãos iremos rodar o projeto e validar se você construiu o projeto atendendo ao cenário proposto e os requisitos.

 

Boa sorte!

 

### Cenário Proposto

 

* Usando a API Java do desafio anterior

* Você deve criar o front-end, usando seus frameworks de preferência ou Javascript/Css puro.

* Você terá que criar uma aplicação web baseado exemplo conceitual (https://dribbble.com/shots/5201742-Intermediary-Tool-Quotes-Page), mas fique livre trabalhá-lo da melhor forma que preferir.

* Sua aplicação deve conter 3 abas que irão agrupar os gastos das seguintes formas: **Lista Geral**, **Por Mês** e **Por Categoria**.

 

### Requisitos

 

* Seu app deve ser responsivo.

* Só devem ser exibidos os meses em que houveram lançamentos.

* Essa tarefa deve ser entregue em um repositório público no Gitlab, o link desse repositório deve ser enviado para Felipe Marques Melo felipe.melo@itau-unibanco.com.br com seu nome completo

 

### Dica

 

* Você pode usar um framework de CSS (como bootstrap), mas não é obrigatório.

* Você pode usar um pré-processador de CSS (como Sass), mas não é obrigatório.

* Você pode usar um framework de JS (como Angular) é recomendado, mas também poderá ser feito em Javascript puro..

* Atente-se a organização do projeto isso será levado em conta.

 

**ATENÇÃO:** _Após o envio do repositório para o e-mail descrito nos requisitos não serão aceitos novos commits, essa atividade deve ser enviada até 20/04/2020 10:00._

 

### Bom Desempenho!

 

### #QueremosVoceNoItau